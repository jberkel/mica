// swift-tools-version:5.2
import PackageDescription

let package = Package(
    name: "mica",
    products: [
        .library(name: "mica", targets: ["mica"]),
        .executable(name: "cli", targets: ["cli"])
    ],
    dependencies: [
        .package(url: "https://github.com/Quick/Quick.git", .upToNextMajor(from: "2.2.0")),
        .package(url: "https://github.com/Quick/Nimble.git", .upToNextMajor(from: "8.0.0")),
        .package(url: "https://github.com/AliSoftware/OHHTTPStubs.git", .upToNextMajor(from: "9.0.0")),
        .package(url: "https://github.com/apple/swift-argument-parser", from: "0.0.1")
    ],
    targets: [
        .target(name: "mica", dependencies: []),
        .target(name: "cli", dependencies: ["mica",
                                            .product(name: "ArgumentParser", package: "swift-argument-parser")]),
        .testTarget(
            name: "micaTests",
            dependencies: ["mica", "Quick", "Nimble", .product(name: "OHHTTPStubsSwift", package: "OHHTTPStubs")])
    ]
)
