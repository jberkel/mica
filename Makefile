mica.xcodeproj/project.pbxproj: Package.swift
	swift package generate-xcodeproj

test:
	swift test

lint:
	swiftlint --strict

ci: lint test

log:
	log stream --predicate "subsystem == 'com.zegoggles.mica'" --debug --info