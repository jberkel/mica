import Foundation
import Quick
import Nimble
@testable import mica

final class WikipediaAPIQueryBuilderSpec: QuickSpec {
    override func spec() {

        describe("WikipediaAPIQueryBuilder") {
            describe("build") {

                it("builds a query for a title and a site") {
                    let result = try! WikipediaAPIQueryBuilder(language: "en", action: .query)
                        .withTitles("Foo")
                        .build()

                    expect(result.absoluteString) == "https://en.wikipedia.org/w/api.php?action=query&titles=Foo&format=json&formatversion=2&errorformat=plaintext"
                }

                it("builds a query for ids") {
                    let result = try! WikipediaAPIQueryBuilder(language: "en", action: .query)
                        .withPageIds(1234)
                        .build()

                    expect(result.absoluteString) == "https://en.wikipedia.org/w/api.php?action=query&pageids=1234&format=json&formatversion=2&errorformat=plaintext"
                }

                context("when no titles nor pageids are specified") {
                    it("errors") {
                        expect {
                            try WikipediaAPIQueryBuilder(language: "en", action: .query)
                                .build()
                        }.to(throwError(errorType: BuilderError.self))
                    }
                }
            }
        }
    }
}
