import Foundation
import Quick
import Nimble
@testable import mica

final class ModelSpec: QuickSpec {
    override func spec() {
        describe("decode") {
            context("Response") {

                func fixture(_ name: String) -> Data {
                    let fileURL = URL(fileURLWithPath: "./fixtures/wikipedia/\(name)")
                    return try! Data(contentsOf: fileURL)
                }

                it("parses redirects") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(Response.self, from: fixture("redirect.json"))

                    expect(response.batchcomplete) == true

                    guard let query = response.query else { fatalError("No query returned") }

                    expect(query.normalized).to(beNil())
                    expect(query.redirects) ==
                        [Redirect(from: "ERTE", to: "Expediente de Regulación Temporal de Empleo")]

                    expect(query.pages) ==
                        [Page(ns: 0,
                              title: "Expediente de Regulación Temporal de Empleo",
                              pageid: 9331763,
                              pageprops: [PageProp.wikibase_item.rawValue: "Q1462531"],
                              description: "Expediente de regulación de empleo",
                              descriptionsource: "central",
                              special: nil,
                              missing: nil,
                              invalid: nil,
                              invalidreason: nil,
                              fullurl: nil,
                              canonicalurl: nil
                        )]
                }

                it("parses missing") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(Response.self, from: fixture("missing.json"))

                    expect(response.batchcomplete) == true
                    expect(response.query?.normalized) ==
                        [Normalized(fromencoded: false, from: "notfound", to: "Notfound")]

                    expect(response.query?.pages) ==
                        [Page(ns: 0,
                              title: "Notfound",
                              pageid: nil,
                              pageprops: nil,
                              description: nil,
                              descriptionsource: nil,
                              special: nil,
                              missing: true,
                              invalid: nil,
                              invalidreason: nil,
                              fullurl: nil,
                              canonicalurl: nil
                         )]
                }

                it("parses invalid title") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(Response.self, from: fixture("invalid.json"))

                    expect(response.batchcomplete) == true
                    expect(response.query?.normalized).to(beNil())
                    expect(response.query?.pages) ==
                        [Page(ns: nil,
                              title: "Talk:",
                              pageid: nil,
                              pageprops: nil,
                              description: nil,
                              descriptionsource: nil,
                              special: nil,
                              missing: nil,
                              invalid: true,
                              invalidreason: Page.InvalidReason(
                                  code: "title-invalid-empty",
                                  text: """
                                        El título de la página solicitada está vacío o contiene solo el nombre de
                                         un espacio de nombres.
                                        """.replacingOccurrences(of: "\n", with: "")
                              ),
                              fullurl: nil,
                              canonicalurl: nil
                        )]
                }

                it("parses special title") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(Response.self, from: fixture("special.json"))

                    expect(response.batchcomplete) == true
                    expect(response.query?.pages) ==
                        [Page(ns: -1,
                              title: "Especial:Versión",
                              pageid: nil,
                              pageprops: nil,
                              description: nil,
                              descriptionsource: nil,
                              special: true,
                              missing: nil,
                              invalid: nil,
                              invalidreason: nil,
                              fullurl: nil,
                              canonicalurl: nil
                        )]

                    expect(response.query?.normalized) ==
                        [Normalized(fromencoded: false, from: "Special:Version", to: "Especial:Versión")]
                }
            }
        }
    }
}
