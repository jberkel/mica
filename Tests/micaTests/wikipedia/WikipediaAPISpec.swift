import Foundation
import Quick
import Nimble
import Combine
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import mica

@available(iOS 13.0, macOS 10.15, *)
final class WikipediaAPISpec: QuickSpec {
    override func spec() {
        describe("execute") {
            var subject: WikipediaAPI!

            beforeEach {
                subject = WikipediaAPI()
            }

            describe("execute") {

                context("successful request") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            fixture(filePath: "fixtures/wikipedia/success.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("parses the response") {
                        var result: Response?
                        let builder = WikipediaAPIQueryBuilder(language: "en", action: .query)

                        let cancellable = subject.execute(builder: builder.withPageIds(1000))
                            .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                if case .failure(let failure) = completion {
                                    fail("Received failure: \(failure)")
                                }
                            }, receiveValue: { response in
                                result = response
                            })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        let page = result?.query?.pages.first

                        expect(page?.title) == "Hercule Poirot"
                        expect(page?.ns) == 0
                        expect(page?.pageid) == 1000

                        cancellable.cancel()
                    }

                }

                context("unsuccessful request (500") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            HTTPStubsResponse(data: Data(), statusCode: 500, headers: [:])
                        }
                    }

                    it("returns invalidResponse") {
                        var result: Subscribers.Completion<Error>?
                        let builder = WikipediaAPIQueryBuilder(language: "en", action: .query)

                        let cancellable = subject.execute(builder: builder.withPageIds(1000))
                                .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                    result = completion
                                }, receiveValue: { _ in })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        guard case .failure(let error as APIError) = result,
                              case .invalidResponse(_) = error else {
                            fatalError("Expected invalidResponse error, got \(String(describing: result))")
                        }
                        cancellable.cancel()
                    }
                }

                context("response with errors") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            fixture(filePath: "fixtures/wikipedia/error_badinteger.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("maps the error to APIError") {
                        var result: Subscribers.Completion<Error>?
                        let builder = WikipediaAPIQueryBuilder(language: "en", action: .query)

                        let cancellable = subject.execute(builder: builder.withPageIds(1000))
                            .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                result = completion
                            }, receiveValue: { _ in })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        guard case .failure(let error as APIError) = result,
                              case .apiError(let apiError) = error else {
                            fatalError("Expected invalidResponse error, got \(String(describing: result))")
                        }

                        expect(apiError.code) == "badinteger"
                        expect(apiError.module) == "query"
                        expect(apiError.text) == "Invalid value \"foo\" for integer parameter \"pageids\"."

                        cancellable.cancel()
                    }
                }
            }
        }
    }
}
