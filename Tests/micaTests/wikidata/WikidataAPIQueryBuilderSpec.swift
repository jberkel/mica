import Foundation
import Quick
import Nimble
@testable import mica

final class WikidataAPIQueryBuilderSpec: QuickSpec {
    override func spec() {

        describe("WikidataAPIQueryBuilder") {
            describe("build") {

                it("builds a query for a title and a site") {
                    let result = try! WikidataAPIQueryBuilder(action: .wbgetentities)
                        .withTitles("Foo")
                        .withSite("enwiki")
                        .withLanguage("en")
                        .build()

                    expect(result.absoluteString) == "https://www.wikidata.org/w/api.php?action=wbgetentities&sites=enwiki&titles=Foo&languages=en&format=json&formatversion=2&errorformat=plaintext"
                }

                it("builds a query for ids") {
                    let result = try! WikidataAPIQueryBuilder(action: .wbgetentities)
                        .withIds("Q42")
                        .build()

                    expect(result.absoluteString) == "https://www.wikidata.org/w/api.php?action=wbgetentities&ids=Q42&format=json&formatversion=2&errorformat=plaintext"
                }

                context("when a title is specified without site") {
                    it("errors") {
                        expect {
                            try WikidataAPIQueryBuilder(action: .wbgetentities).withTitles("Foo").build()
                        }.to(throwError(errorType: BuilderError.self))
                    }
                }

                context("when multiple titles are specified for multiple sites") {
                    it("errors") {
                        expect {
                            try WikidataAPIQueryBuilder(action: .wbgetentities)
                                .withTitles("Foo", "Bar")
                                .withSite("enwiki", "frwiki")
                                .build()
                        }.to(throwError(errorType: BuilderError.self))
                    }
                }

                context("when neither titles nor ids are specified") {
                    it("errors") {
                        expect {
                            try WikidataAPIQueryBuilder(action: .wbgetentities)
                                .withSite("enwiki", "frwiki")
                                .build()
                        }.to(throwError(errorType: BuilderError.self))
                    }
                }
            }
        }
    }
}
