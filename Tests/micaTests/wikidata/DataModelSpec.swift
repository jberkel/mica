import Foundation
import Quick
import Nimble
@testable import mica

final class DataModelSpec: QuickSpec {
    override func spec() {
        describe("DataResponse") {
            describe("decode") {

                func fixture(_ name: String) -> Data {
                    let fileURL = URL(fileURLWithPath: "./fixtures/wikidata/\(name)")
                    return try! Data(contentsOf: fileURL)
                }

                it("parses success") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(DataResponse.self, from: fixture("success.json"))

                    expect(response.success) == 1
                    expect(response.entities) == [
                        Entity(type: "item",
                               id: "Q1462531",
                               labels: [Label(language: "es", value: "ERE")],
                               descriptions: [Description(language: "es",
                                                          value: "Expediente de regulación de empleo")],
                               sitelinks: [SiteLink(site: "eswiki",
                                                    title: "Expediente de Regulación Temporal de Empleo",
                                                    badges: ["Q17437796"],
                                                    url: "https://es.wikipedia.org/wiki/Expediente_de_Regulaci%C3%B3n_Temporal_de_Empleo")])
                    ]
                    expect(response.normalized) ==
                        DataResponse.Normalized(from: "ERTE",
                                                to: "Expediente de Regulación Temporal de Empleo")
                }

                it("parses language fallback") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(DataResponse.self, from: fixture("language-fallback.json"))

                    expect(response.success) == 1
                    expect(response.entities) == [
                        Entity(type: "item",
                               id: "Q51104430",
                               labels: [Label(language: "es", value: "David Grusky")],
                               descriptions: [Description(language: "en",
                                                          value: "sociologist",
                                                          forLanguage: "es")],
                               sitelinks: [SiteLink(site: "enwiki",
                                                   title: "David Grusky",
                                                   badges: [],
                                                   url: "https://en.wikipedia.org/wiki/David_Grusky")])
                   ]
                }

                it("parses errors") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(DataResponse.self, from: fixture("error.json"))
                    expect(response.success).to(beNil())
                    expect(response.entities).to(beNil())

                    expect(response.warnings) == [
                        APIResponseWarning(code: "unrecognizedvalues",
                                   text: "Unrecognized value for parameter \"sites\": xxxi",
                                   module: "wbgetentities")
                    ]
                    expect(response.errors) == [
                        APIResponseError(code: "param-missing",
                                 text: """
                                       Either provide the Item \"ids\" or pairs of \"sites\" and \"titles\"
                                        for corresponding page
                                       """.replacingOccurrences(of: "\n", with: ""),
                                 module: "main")
                    ]
                }

                it("parses missing") {
                    let decoder = JSONDecoder()
                    let response = try! decoder.decode(DataResponse.self, from: fixture("missing.json"))
                    expect(response.success) == 1
                    expect(response.entities) == []
                }
            }
        }
    }
}
