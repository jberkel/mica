import Foundation
import Quick
import Nimble
import Combine
import OHHTTPStubs
import OHHTTPStubsSwift
@testable import mica

@available(iOS 13.0, macOS 10.15, *)
final class WikidataAPISpec: QuickSpec {
    override func spec() {
        describe("execute") {
            var subject: WikidataAPI!

            beforeEach {
                subject = WikidataAPI()
            }

            describe("execute") {
                context("successful request") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            fixture(filePath: "fixtures/wikidata/Q2.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("parses the response") {
                        var result: DataResponse?
                        let builder = WikidataAPIQueryBuilder(action: .wbgetentities)
                        let cancellable = subject.execute(builder: builder.withIds("Q2"))
                            .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                if case .failure(let failure) = completion {
                                    fail("Received failure: \(failure)")
                                }
                            }, receiveValue: { response in
                                result = response
                            })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))

                        expect(result?.entities?.first?.id) == "Q2"
                        let descriptionEn = result?.entities?.first?.descriptions.first { $0.language == "en" }
                        expect(descriptionEn?.value) == "third planet from the Sun in the Solar System"

                        cancellable.cancel()
                    }
                }

                context("unsuccessful request (500") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            HTTPStubsResponse(data: Data(), statusCode: 500, headers: [:])
                        }
                    }

                    it("returns invalidResponse") {
                        var result: Subscribers.Completion<Error>?
                        let builder = WikidataAPIQueryBuilder(action: .wbgetentities)
                        let cancellable = subject.execute(builder: builder.withIds("Q42"))
                                .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                    result = completion
                                }, receiveValue: { _ in })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        guard case .failure(let error as APIError) = result,
                              case .invalidResponse(let response) = error else {
                            fatalError("Expected invalidResponse")
                        }
                        expect(response?.statusCode) == 500
                        cancellable.cancel()
                    }
                }

                context("unsuccessful request (API error)") {
                    beforeEach {
                        stub(condition: isMethodGET()) { _ in
                            fixture(filePath: "fixtures/wikidata/error.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("maps the API error to APIError") {
                        var result: Subscribers.Completion<Error>?
                        let builder = WikidataAPIQueryBuilder(action: .wbgetentities)
                        let cancellable = subject.execute(builder: builder.withIds("Q42"))
                                .sink(receiveCompletion: { (completion: Subscribers.Completion<Error>) in
                                    result = completion
                                }, receiveValue: { _ in })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        guard case .failure(let error as APIError) = result,
                              case .apiError(let apiError) = error else {
                            fatalError("Expected apiError, got \(String(describing: result))")
                        }
                        expect(apiError.code) == "param-missing"
                        expect(apiError.module) == "main"
                        expect(apiError.text) == "Either provide the Item \"ids\" or pairs of \"sites\" and \"titles\" for corresponding page"

                        cancellable.cancel()
                    }
                }
            }
        }
    }
}
