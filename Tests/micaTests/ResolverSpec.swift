import Foundation
import Quick
import Nimble
import OHHTTPStubs
import OHHTTPStubsSwift
import Combine
@testable import mica

@available(iOS 13.0, macOS 10.15, *)
final class ResolverSpec: QuickSpec {
    override func spec() {

        describe("Resolver") {
            var subject: Resolver!

            beforeEach {
                subject = Resolver()
                HTTPStubs.removeAllStubs()
            }

            describe("resolve") {
                context("unambiguous lookup") {
                    beforeEach {
                        stub(condition: isHost("www.wikidata.org")) { _ in
                            fixture(filePath: "fixtures/resolver/girona.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("resolves to one entity") {
                        var result: Resolver.Result?
                        let cancellable = subject.resolve(
                            title: "girona",
                            site: "cawiki",
                            languages: ["ca", "en"]
                        ).sink(
                            receiveCompletion: { completion in
                                if case .failure(let failure) = completion {
                                    fail("Received failure: \(failure)")
                                }
                            }, receiveValue: { result = $0 })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        expect(result?.title) == "girona"
                        expect(result?.normalizedTitle) == "Girona"
                        expect(result?.description) == "Girona"
                        expect(result?.entities.count) == 1
                        expect(result?.entities.first?.id) == "Q7038"

                        let sitelinks = result?.entities.first?.sitelinks
                        expect(sitelinks?.map(\.url)).to(contain(
                            "https://ca.wikipedia.org/wiki/Girona",
                            "https://en.wikipedia.org/wiki/Girona"
                        ))
                        cancellable.cancel()
                    }
                }

                context("ambiguous lookup") {
                    beforeEach {
                        stub(condition: isHost("www.wikidata.org")) { _ in
                            fixture(filePath: "fixtures/resolver/VOX-wikidata.json",
                                    headers: ["Content-Type": "application/json"])
                        }

                        stub(condition: isHost("es.wikipedia.org")) { _ in
                            fixture(filePath: "fixtures/resolver/VOX-wikipedia.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("resolves to multiple entities") {
                        var result: Resolver.Result?
                        let cancellable = subject.resolve(
                            title: "VOX",
                            site: "eswiki",
                            languages: ["es", "en"]
                        ).sink(
                            receiveCompletion: { completion in
                                if case .failure(let failure) = completion {
                                    fail("Received failure: \(failure)")
                                }
                            }, receiveValue: { result = $0 })
                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))
                        expect(result?.title) == "VOX"
                        expect(result?.normalizedTitle) == "Vox"
                        expect(result?.entities.count) == 7

                        let top = result!.entities[0..<3]

                        expect(top.flatMap(\.sitelinks).map(\.title)) == [
                            "Vox (partido político)",
                            "Vox (sitio web)",
                            "Vox (red social)"
                        ]

                        expect(top.flatMap(\.sitelinks).map(\.url)) == [
                            "https://es.wikipedia.org/wiki/Vox_(partido_pol%C3%ADtico)",
                            "https://es.wikipedia.org/wiki/Vox_(sitio_web)",
                            "https://es.wikipedia.org/wiki/Vox_(red_social)"
                        ]

                        expect(top.flatMap(\.descriptions).map(\.value)) == [
                            "partido político español",
                            "sitio de noticias y opinión estadounidense",
                            "servicio de blogs de Internet"
                        ]

                        expect(top.flatMap(\.labels).map(\.value)) == [
                            "Vox (partido político)",
                            "Vox (sitio web)",
                            "Vox (red social)"
                        ]
                        cancellable.cancel()
                    }
                }

                context("when no results are returned") {
                    beforeEach {
                        stub(condition: isHost("www.wikidata.org")) { _ in
                            fixture(filePath: "fixtures/resolver/doesnotexist.json",
                                    headers: ["Content-Type": "application/json"])
                        }
                    }

                    it("returns ResolverError.notFound") {
                        var completion: Subscribers.Completion<Error>?
                        let cancellable = subject.resolve(
                            title: "DOESNOTEXIST",
                            site: "eswiki",
                            languages: ["es"]
                        ).sink(
                            receiveCompletion: { completion = $0 },
                            receiveValue: { _ in fail("received unexpected value") })

                        RunLoop.main.run(until: Date().addingTimeInterval(0.1))

                        guard case .failure(let error as ResolverError) = completion else {
                            fatalError("Unexpected completion: \(String(describing: completion))")
                        }

                        expect(error) == .notFound
                        cancellable.cancel()
                    }
                }
            }
        }
    }
}
