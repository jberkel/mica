import Foundation

public enum APIError: Error, LocalizedError, Equatable {
    case invalidURL
    case invalidResponse(HTTPURLResponse?)
    case apiError(APIResponseError)

    public var errorDescription: String? {
        switch self {
        case .invalidURL: return "Invalid URL"
        case .invalidResponse(let response): return "Invalid Response: \(String(describing: response))"
        case .apiError(let error): return "API error: \(error)"
        }
    }
}
