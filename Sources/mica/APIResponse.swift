import Foundation

public protocol APIResponse: Decodable {
    var errors: [APIResponseError]? { get }
    var warnings: [APIResponseWarning]? { get }
}

// https://www.mediawiki.org/wiki/API:Errors_and_warnings
public struct APIResponseWarning: Decodable, Equatable {
    public let code: String
    public let text: String
    public let module: String
}

public struct APIResponseError: Decodable, Equatable {
    public let code: String
    public let text: String
    public let module: String
}
