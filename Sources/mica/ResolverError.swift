import Foundation

public enum ResolverError: Error, LocalizedError, Equatable {
    case notFound
    case ambiguous
    case invalidParameter

    public var errorDescription: String? {
        switch self {
        case .notFound: return "Not found"
        case .ambiguous: return "Ambiguous"
        case .invalidParameter: return "Invalid parameter"
        }
    }
}
