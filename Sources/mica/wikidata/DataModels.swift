import Foundation
// swiftlint:disable nesting
public typealias WikidataItemId = String

public struct Label: Decodable, Equatable {
    public let language: String
    public let value: String
    // language fallback
    public let forLanguage: String?

    init(language: String, value: String, forLanguage: String? = nil) {
        self.language = language
        self.value = value
        self.forLanguage = forLanguage
    }

    public init(decoder: Decoder) throws {
        let topLevel = try decoder.container(keyedBy: CodingKeys.self)
        self.language = try topLevel.decode(String.self, forKey: .language)
        self.forLanguage = try topLevel.decodeIfPresent(String.self, forKey: .forLanguage)
        self.value = try topLevel.decode(String.self, forKey: .value)
    }

    internal enum CodingKeys: String, CodingKey {
        case language
        case value
        case forLanguage = "for-language"
    }
}

public struct Description: Decodable, Equatable {
    public let language: String
    public let value: String
    // language fallback
    public let forLanguage: String?

    init(language: String, value: String, forLanguage: String? = nil) {
        self.language = language
        self.value = value
        self.forLanguage = forLanguage
    }

    public init(decoder: Decoder) throws {
        let topLevel = try decoder.container(keyedBy: CodingKeys.self)
        self.language = try topLevel.decode(String.self, forKey: .language)
        self.forLanguage = try topLevel.decodeIfPresent(String.self, forKey: .forLanguage)
        self.value = try topLevel.decode(String.self, forKey: .value)
    }

    internal enum CodingKeys: String, CodingKey {
        case language
        case value
        case forLanguage = "for-language"
    }
}

public struct SiteLink: Decodable, Equatable {
    public let site: String
    public let title: String
    public let badges: [String]
    public let url: String?      // only set when using Prop.sitelinksUrls
}

public struct Entity: Decodable, Equatable {
    public let type: String
    public let id: String
    public let labels: [Label]
    public let descriptions: [Description]
    public let sitelinks: [SiteLink]

    internal enum CodingKeys: String, CodingKey {
        case type
        case id
        case labels
        case descriptions
        case sitelinks
    }

    init(type: String, id: String,
         labels: [Label],
         descriptions: [Description],
         sitelinks: [SiteLink]) {
        self.type = type
        self.id = id
        self.labels = labels
        self.descriptions = descriptions
        self.sitelinks = sitelinks
    }

    public init(from decoder: Decoder) throws {
        let topLevel = try decoder.container(keyedBy: CodingKeys.self)
        self.type = try topLevel.decode(String.self, forKey: .type)
        self.id = try topLevel.decode(String.self, forKey: .id)
        let labels = try topLevel.nestedContainer(keyedBy: LanguageKey.self, forKey: .labels)
        self.labels = try labels.allKeys.map { languageKey in
            try labels.decode(Label.self, forKey: languageKey)
        }
        let descriptions = try topLevel.nestedContainer(keyedBy: LanguageKey.self, forKey: .descriptions)
        self.descriptions = try descriptions.allKeys.map { languageKey in
            try descriptions.decode(Description.self, forKey: languageKey)
        }
        let sitelinks = try topLevel.nestedContainer(keyedBy: SiteKey.self, forKey: .sitelinks)
        self.sitelinks = try sitelinks.allKeys.map { siteKey in
            try sitelinks.decode(SiteLink.self, forKey: siteKey)
        }
    }
}

public struct DataResponse: APIResponse, Equatable {

    public struct Normalized: Decodable, Equatable {
        public let from: String
        public let to: String

        public init(from: String, to: String) {
            self.from = from
            self.to = to
        }

        internal enum CodingKeys: String, CodingKey {
            case n
            enum Internal: String, CodingKey {
                case from, to
            }
        }

        public init(from decoder: Decoder) throws {
            let topLevel = try decoder.container(keyedBy: CodingKeys.self)
            let container = try topLevel.nestedContainer(keyedBy: CodingKeys.Internal.self, forKey: .n)
            self.from = try container.decode(String.self, forKey: .from)
            self.to = try container.decode(String.self, forKey: .to)
        }
    }

    public let warnings: [APIResponseWarning]?
    public let errors: [APIResponseError]?
    public let normalized: Normalized?
    public let entities: [Entity]?
    public let success: Int?

    internal enum CodingKeys: String, CodingKey {
        case warnings
        case errors
        case normalized
        case success
        case entities
    }

    internal struct IdKey: CodingKey {
        let id: String
        var intValue: Int?
        var stringValue: String { id }

        init?(stringValue: String) {
            self.id = stringValue
        }

        init?(intValue: Int) { nil }
    }

    public init(from decoder: Decoder) throws {
        let topLevel = try decoder.container(keyedBy: CodingKeys.self)
        self.warnings = try topLevel.decodeIfPresent([APIResponseWarning].self, forKey: .warnings)
        self.errors = try topLevel.decodeIfPresent([APIResponseError].self, forKey: .errors)
        self.success = try topLevel.decodeIfPresent(Int.self, forKey: .success)

        if self.success == 1 {
            let entities = try topLevel.nestedContainer(keyedBy: IdKey.self, forKey: .entities)
            self.normalized = try topLevel.decodeIfPresent(Normalized.self, forKey: .normalized)
            self.entities = try entities.allKeys.filter { !$0.stringValue.starts(with: "-") }.map { idKey in
                try entities.decode(Entity.self, forKey: idKey)
            }
        } else {
            self.entities = nil
            self.normalized = nil
        }
    }
}

internal struct LanguageKey: CodingKey {
    let language: String
    var intValue: Int?
    var stringValue: String { language }

    init?(stringValue: String) {
        self.language = stringValue
    }
    init?(intValue: Int) { nil }
}

internal struct SiteKey: CodingKey {
    let language: String
    var intValue: Int?
    var stringValue: String { language }

    init?(stringValue: String) {
        self.language = stringValue
    }
    init?(intValue: Int) { nil }
}
