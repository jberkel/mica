import Foundation

// https://www.mediawiki.org/wiki/Wikibase/API
public class WikidataAPIQueryBuilder: QueryBuilder {
    private var ids: [String] = []
    private var titles: [String] = []
    private var sites: [String] = []
    private var languages: [String] = []
    private var languageFallback: Bool = false
    private var siteFilter: [String] = []
    private var props: [Prop] = []
    private var redirects: Bool = false
    private var normalize: Bool = false
    private let action: Action

    public init(action: Action) {
        self.action = action
    }

    public enum Action: String, RawRepresentable {
        /// Gets the data for multiple Wikibase entities
        /// https://www.wikidata.org/w/api.php?action=help&modules=wbgetentities
        case wbgetentities

        func validate(builder: WikidataAPIQueryBuilder) throws {
            switch self {
            case .wbgetentities:
                // The title of the corresponding page Use together with sites, but only give one site for several
                // titles or several sites for one title.
                if builder.titles.count == 1 && builder.sites.count < 1 {
                    throw BuilderError.invalidParameter("Need to provide at least one site for a title")
                }
                if builder.titles.count > 1 && builder.sites.count != 1 {
                    throw BuilderError.invalidParameter("Give exactly one site for multiple titles")
                }
                if builder.sites.count > 1 && builder.titles.count != 1 {
                    throw BuilderError.invalidParameter("Give exactly one title for multiple sites")
                }
                if builder.titles.isEmpty && builder.ids.isEmpty {
                    throw BuilderError.invalidParameter("Need to provide either ids or titles")
                }
            }
        }
    }

    public enum Prop: String, RawRepresentable {
        case aliases
        case claims
        case datatype
        case descriptions
        case info
        case labels
        case sitelinks
        case sitelinksUrls = "sitelinks/urls"
    }

    // The IDs of the entities to get the data from
    public func withIds(_ ids: String...) -> Self {
        self.ids = ids
        return self
    }

    // The title of the corresponding page Use together with sites
    public func withTitles(_ titles: String...) -> Self {
        self.titles = titles
        return self
    }

    // By default the internationalized values are returned in all available languages. This parameter allows
    // filtering these down to one or more languages by providing one or more language codes.
    public func withLanguages(_ languages: [String]) -> Self {
        self.languages = languages
        return self
    }

    public func withLanguage(_ languages: String...) -> Self {
        withLanguages(languages)
    }

    // Apply language fallback for languages defined in the languages parameter, with the current
    // context of API call.
    public func withLanguageFallback() -> Self {
        self.languageFallback = true
        return self
    }

    // Identifier for the site on which the corresponding page resides Use together with title, but only give
    // one site for several titles or several sites for one title.
    public func withSite(_ sites: String...) -> Self {
        withSites(sites)
    }

    public func withSites(_ sites: [String]) -> Self {
        self.sites = sites
        return self
    }

    // Filter sitelinks in entities to those with these site IDs.
    public func siteFilter(_ sites: [String]) -> Self {
        self.siteFilter = sites
        return self
    }

    // Automatically resolve redirects
    public func resolveRedirects() -> Self {
        self.redirects = true
        return self
    }

    // Try to normalize the page title against the client site. This only works if exactly one site and
    // one page have been given.
    public func normalizePageTitle() -> Self {
        self.normalize = true
        return self
    }

    // The names of the properties to get back from each entity.
    public func props(_ props: [Prop]) -> Self {
        self.props = props
        return self
    }

    public func build() throws -> URL {
        try validateParameters()

        var components = URLComponents()
        components.scheme = "https"
        components.host = "www.wikidata.org"
        components.path = "/w/api.php"
        components.queryItems = [
            URLQueryItem(name: "action", value: action.rawValue),
            ids.count > 0 ? URLQueryItem(name: "ids", value: ids.joined(separator: "|")) : nil,
            sites.count > 0 ? URLQueryItem(name: "sites", value: sites.joined(separator: "|")) : nil,
            titles.count > 0 ? URLQueryItem(name: "titles", value: titles.joined(separator: "|")) : nil,
            redirects ? URLQueryItem(name: "redirects", value: "yes") : nil,
            normalize ? URLQueryItem(name: "normalize", value: "1") : nil,
            props.count > 0 ? URLQueryItem(name: "props",
                                           value: props.map { $0.rawValue }.joined(separator: "|")) : nil,
            siteFilter.count > 0 ? URLQueryItem(name: "sitefilter", value: siteFilter.joined(separator: "|")) : nil,
            languages.count > 0 ? URLQueryItem(name: "languages", value: languages.joined(separator: "|")) : nil,
            languageFallback ? URLQueryItem(name: "languagefallback", value: "") : nil,
            // JSON format
            URLQueryItem(name: "format", value: "json"),
            // Modern format. Returns responses in a cleaner format, encodes most non-ASCII characters as UTF-8.
            URLQueryItem(name: "formatversion", value: "2"),
            // Intended for human display in HTML-incapable clients.
            URLQueryItem(name: "errorformat", value: "plaintext")
        ].compactMap { $0 }

        if let url = components.url {
            return url
        } else {
            throw BuilderError.invalidURL
        }
    }

    private func validateParameters() throws {
        try action.validate(builder: self)
    }
}
