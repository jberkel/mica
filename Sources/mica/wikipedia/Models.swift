import Foundation

public struct Response: APIResponse, Equatable {

    public struct Query: Decodable, Equatable {
        public let redirects: [Redirect]?
        public let normalized: [Normalized]?
        public let pages: [Page]
    }

    public let errors: [APIResponseError]?
    public let warnings: [APIResponseWarning]?

    // Indicates that all data for the current batch of items has been returned.
    public let batchcomplete: Bool?
    public let query: Query?
}

public struct Page: Decodable, Equatable {
    public let ns: Int?
    public let title: String
    public let pageid: Int64?
    public let pageprops: [String: String]? // [PageProp: String]?
    public let description: String?
    // central (wikidata) / local ({{SHORTDESC:}})
    // https://www.mediawiki.org/wiki/Extension:ShortDescription
    public let descriptionsource: String?

    public let special: Bool?
    // Titles that don't exist or are invalid will have a missing or invalid attribute set in the response.
    public let missing: Bool?
    public let invalid: Bool?
    public let invalidreason: InvalidReason?

    // prop=info
    public let fullurl: String?
    public let canonicalurl: String?

    public struct InvalidReason: Decodable, Equatable {
        public let code: String
        public let text: String
    }

    func has(property: PageProp) -> Bool {
        pageprops?.keys.contains(property.rawValue) ?? false
    }
}

public struct Redirect: Decodable, Equatable {
    public let from: String
    public let to: String
}

// Title normalization converts page titles to their canonical form.
// This means capitalizing the first character, replacing underscores with spaces, and changing namespace to the
// localized form defined for that wiki.
public struct Normalized: Decodable, Equatable {
    public let fromencoded: Bool
    public let from: String
    public let to: String
}

/// https://www.mediawiki.org/wiki/API:Properties
public enum Prop: String, RawRepresentable {
    case categories     // List all categories the pages belong to.
    case categoryinfo   // Returns information about the given categories.
    case cirrusbuilddoc // Dump of a CirrusSearch article document from the database servers
    case cirruscompsuggestbuilddoc // Dump of the document used by the completion suggester
    case cirrusdoc      // Dump of a CirrusSearch article document from the search servers
    case contributors   // Get the list of logged-in contributors and the count of anonymous contributors to a page.
    case deletedrevisions // Get deleted revision information.
    case duplicatefiles  // List all files that are duplicates of the given files based on hash values.
    case extlinks        // Returns all external URLs (not interwikis) from the given pages.
    case extracts        // Returns plain-text or limited HTML extracts of the given pages.
    case fileusage       // Find all pages that use the given files.
    case globalusage     // Returns global image usage for a certain image.
    case imageinfo       // Returns file information and upload history.
    case images          // Returns all files contained on the given pages.
    case info            // Get basic page information.
                         // See "InfoProp" for parameters
    case iwlinks         // Returns all interwiki links from the given pages.
    case langlinks       // Returns all interlanguage links from the given pages.
    case links           // Returns all links from the given pages.
    case linkshere       // Find all pages that link to the given pages.
    case pageimages      // Returns information about images on the page, such as thumbnail and presence of photos.
    case pageprops       // Get various page properties defined in the page content.
    case pageterms       // Get the Wikidata terms (typically labels, descriptions and aliases) associated with a
                         // page via a sitelink. On the entity page itself, the terms are used directly.
                         // Caveat: On a repo wiki, this module only works directly on entity pages, not on pages
                         // connected to an entity via a sitelink.
                         // This case may change in the future.
    case pageviews       // Shows per-page pageview data (the number of daily pageviews for each of the last
                         // pvipdays days).
    case redirects       // Returns all redirects to the given pages.
    case revisions       // Get revision information.
    case stashimageinfo  // Returns file information for stashed files.
    case templates       // Returns all pages transcluded on the given pages.
    case transcludedin   // Find all pages that transclude the given pages.
    case transcodestatus // Get transcode status for a given file page.
    case videoinfo       // Extends imageinfo to include video source (derivatives) information
    case wbentityusage   // Returns all entity IDs used in the given pages.

    // internal properties
    case description  // Get a short description a.k.a. subtitle explaining what the target page is about.
    case mapdata      // Request all Kartographer map data for the given pages
}

// swiftlint:disable identifier_name
// https://www.mediawiki.org/w/api.php?action=query&list=pagepropnames
public enum PageProp: String, RawRepresentable {
    case defaultsort
    case disambiguation
    case displaytitle
    case expectunusedcategory
    case forcetoc
    case graph_specs
    case hiddencat
    case index
    case newsectionlink
    case noeditsection
    case noindex
    case nonewsectionlink
    case notoc
    case page_image_free
    case page_top_level_section_count
    case score
    case templatedata
    case useliquidthreads = "use-liquid-threads"
    case wikibase_item
}

/// https://www.mediawiki.org/wiki/API:Info
public enum InfoProp: String, RawRepresentable {
    case protection // List the protection level of each page.
    case talkid     // The page ID of the talk page for each non-talk page.
    case watched    // List the watched status of each page.
    case watchers   // The number of watchers, if allowed.
    case visitingwatchers      // The number of watchers of each page who have visited recent edits to
                               // that page, if allowed.
    case notificationtimestamp // The watchlist notification timestamp of each page.
    case subjectid      // The page ID of the parent page for each talk page.
    case url            // Gives a full URL, an edit URL, and the canonical URL for each page.
    case readable       // Deprecated. Whether the user can read this page. Use intestactions=read instead.
    case preload        // Gives the text returned by EditFormPreloadText.
    case displaytitle   // Gives the manner in which the page title is actually displayed.
    case varianttitles  // Gives the display title in all variants of the site content language.

    case intestactions  // Test whether the current user can perform certain actions on the page.
    case intestactionsdetail // Detail level for intestactions.

    case incontinue     // When more results are available, use this to continue.
}
