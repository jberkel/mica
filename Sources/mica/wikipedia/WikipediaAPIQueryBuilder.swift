import Foundation

// https://www.mediawiki.org/wiki/API:Main_page
public class WikipediaAPIQueryBuilder: QueryBuilder {
    private var pageids: [Int] = []
    private var titles: [String] = []
    private var generator: Generator?
    private var props: [Prop] = []
    private var infoProps: [InfoProp] = []
    private var pageProps: [PageProp] = []
    private var redirects: Bool = false
    private var normalize: Bool = false
    private let action: Action
    private let language: String

    public init(language: String, action: Action) {
        self.language = language
        self.action = action
    }

    public enum Action: String, RawRepresentable {
        /// The action=query module allows you to fetch information about a wiki and the data stored in it,
        /// such as the wikitext of a particular page, the links and categories of a set of pages, or the
        /// token you need to change wiki content.
        /// [Query module](https://www.mediawiki.org/w/api.php?action=help&modules=query)
        case query

        func validate(builder: WikipediaAPIQueryBuilder) throws {
            switch self {
            case .query:
                if builder.pageids.isEmpty && builder.titles.isEmpty {
                    throw BuilderError.invalidParameter("Need to specify either page ids or titles")
                }
            }
        }
    }

    /// The IDs of the entities to get the data from
    public func withPageIds(_ pageIds: Int...) -> Self {
        self.pageids = pageIds
        return self
    }

    /// The title of the corresponding page Use together with sites
    public func withTitles(_ titles: String...) -> Self {
        self.titles = titles
        return self
    }

    public func withGenerator(_ generator: Generator) -> Self {
        self.generator = generator
        return self
    }

    /// Automatically resolve redirects
    public func resolveRedirects() -> Self {
        self.redirects = true
        return self
    }

    /// Try to normalize the page title against the client site. This only works if exactly one site and
    /// one page have been given.
    public func normalizePageTitle() -> Self {
        self.normalize = true
        return self
    }

    // Which properties to get for the queried pages.
    public func props(_ props: [Prop], infoProps: [InfoProp] = []) -> Self {
        self.props = props
        self.infoProps = infoProps
        return self
    }

    /// Only list these page properties
    /// [List](https://www.mediawiki.org/w/api.php?action=query&list=pagepropnames&ppnlimit=500)
    public func pageProps(_ pageProps: [PageProp]) -> Self {
        self.pageProps = pageProps
        return self
    }

    public func build() throws -> URL {
        try validateParameters()

        var components = URLComponents()
        components.scheme = "https"
        components.host = "\(language).wikipedia.org"
        components.path = "/w/api.php"
        components.queryItems = [
            // Gets the data for multiple Wikibase entities
            URLQueryItem(name: "action", value: action.rawValue),
            pageids.count > 0 ? URLQueryItem(name: "pageids",
                                             value: pageids.map(String.init).joined(separator: "|")) : nil,
            titles.count > 0 ? URLQueryItem(name: "titles", value: titles.joined(separator: "|")) : nil,
            generator.map { URLQueryItem(name: "generator", value: $0.rawValue)  },
            redirects ? URLQueryItem(name: "redirects", value: "yes") : nil,
            normalize ? URLQueryItem(name: "normalize", value: "1") : nil,
            props.count > 0 ? URLQueryItem(name: "prop",
                                           value: props.map(\.rawValue).joined(separator: "|")) : nil,
            infoProps.count > 0 ? URLQueryItem(name: "inprop",
                                               value: infoProps.map(\.rawValue).joined(separator: "|")) : nil,
            pageProps.count > 0 ? URLQueryItem(name: "ppprop",
                                               value: pageProps.map(\.rawValue).joined(separator: "|")) : nil,

            // JSON format
            URLQueryItem(name: "format", value: "json"),
            // Modern format. Returns responses in a cleaner format, encodes most non-ASCII characters as UTF-8.
            URLQueryItem(name: "formatversion", value: "2"),
            // Intended for human display in HTML-incapable clients.
            URLQueryItem(name: "errorformat", value: "plaintext")
        ].compactMap { $0 }

        if let url = components.url {
            return url
        } else {
            throw BuilderError.invalidURL
        }
    }

    private func validateParameters() throws {
        try action.validate(builder: self)
    }

    /// Use generators if you want to get data about a set of pages.
    /// When using a list module as a generator, you don't need to specify the pages.
    /// However, for a property module, you should specify the pages which the generator will work on.
    /// For example, to load all pages that are linked to from the main page, use
    /// `generator=links&titles=Main%20Page.`
    public enum Generator: String, RawRepresentable {
        case allcategories    		// Enumerate all categories.
        case alldeletedrevisions    // List all deleted revisions by a user or in a namespace.
        case allfileusages    		// List all file usages, including non-existing.
        case allimages    		    // Enumerate all images sequentially.
        case alllinks    		    // Enumerate all links that point to a given namespace.
        case allpages    		    // Enumerate all pages sequentially in a given namespace.
        case allredirects    		// List all redirects to a namespace.
        case allrevisions    		// List all revisions.
        case alltransclusions       // List all transclusions (pages embedded using {{x}}), including non-existing.
        case backlinks    		    // Find all pages that link to the given page.
        case categories    	     	// List all categories the pages belong to.
        case categorymembers    	// List all pages in a given category.
        case deletedrevisions    	// Get deleted revision information.
        case duplicatefiles    		// List all files that are duplicates of the given files based on hash values.
        case embeddedin    		    // Find all pages that embed (transclude) the given title.
        case exturlusage    		// Enumerate pages that contain a given URL.
        case fileusage    		    // Find all pages that use the given files.
        case images    		        // Returns all files contained on the given pages.
        case imageusage    		    // Find all pages that use the given image title.
        case iwbacklinks    		// Find all pages that link to the given interwiki link.
        case langbacklinks    		// Find all pages that link to the given language link.
        case links    		        // Returns all links from the given pages.
        case linkshere    		    // Find all pages that link to the given pages.
        case messagecollection    	// Query MessageCollection about translations.
        case mostviewed    		    // Lists the most viewed pages (based on last day's pageview count).
        case pageswithprop    		// List all pages using a given page property.
        case prefixsearch    		// Perform a prefix search for page titles.
        case protectedtitles    	// List all titles protected from creation.
        case querypage    		    // Get a list provided by a QueryPage-based special page.
        case random    		        // Get a set of random pages.
        case recentchanges    		// Enumerate recent changes.
        case redirects    		    // Returns all redirects to the given pages.
        case revisions    		    // Get revision information.
        case search    		        // Perform a full text search.
        case templates    		    // Returns all pages transcluded on the given pages.
        case transcludedin    		// Find all pages that transclude the given pages.
        case watchlist    		    // Get recent changes to pages in the current user's watchlist.
        case watchlistraw    		// Get all pages on the current user's watchlist.
        case wblistentityusage    	// Returns all pages that use the given entity IDs.
        case readinglistentries    	// Internal. List the pages of a certain list
    }
}
