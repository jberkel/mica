import Foundation

public protocol QueryBuilder {
    func build() throws -> URL
}

public enum BuilderError: Error {
    case invalidURL
    case invalidParameter(String)
}
