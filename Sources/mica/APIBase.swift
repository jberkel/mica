#if canImport(Combine)
import Foundation
import Combine
import os.log

@available(iOS 13.0, macOS 10.15, *)
public class APIBase<ResponseType: APIResponse> {
    private let urlSession: URLSession

    public convenience init() {
        self.init(urlSession: URLSession(configuration: URLSessionConfiguration.default))
    }

    public init(urlSession: URLSession) {
        self.urlSession = urlSession
    }

    public func execute(builder: QueryBuilder) -> AnyPublisher<ResponseType, Error> {
        do {
            let url = try builder.build()
            os_log("GET %{public}@", log: log, type: .info, url.absoluteString)
            return urlSession.dataTaskPublisher(for: url)
                .tryMap { data, response -> Data in
                    guard let httpResponse = response as? HTTPURLResponse else {
                        throw APIError.invalidResponse(nil)
                    }
                    guard  httpResponse.statusCode == 200 else {
                        throw APIError.invalidResponse(httpResponse)
                    }
                    return data
                }
                .decode(type: ResponseType.self, decoder: JSONDecoder())
                .tryMap { response -> ResponseType in
                    if let error = response.errors?.first {
                        throw APIError.apiError(error)
                    }
                    if let warnings = response.warnings {
                        warnings.forEach { warning in
                            os_log("Warning: %{public}@", log: self.log, type: .error, warning.text)
                        }
                    }
                    return response
                }
                .eraseToAnyPublisher()
        } catch {
            return Fail<ResponseType, Error>(error: error).eraseToAnyPublisher()
        }
    }

    private var log: OSLog {
        OSLog(subsystem: "com.zegoggles.mica", category: String(describing: Self.self))
    }
}

#endif
