#if canImport(Combine)
import Foundation
import Combine
import os.log

@available(iOS 13.0, macOS 10.15, *)
public class Resolver {
    private let wikidataAPI: WikidataAPI
    private let wikipediaAPI: WikipediaAPI
    private let ignoredItems: [WikidataItemId]

    public struct Result: CustomStringConvertible {
        /// the original query
        public let title: String
        /// the normalized query
        public let normalizedTitle: String?
        public let language: String

        public let entities: [Entity]

        public var description: String {
            normalizedTitle ?? title
        }
    }

    public init(urlSession: URLSession = URLSession(configuration: URLSessionConfiguration.default),
                ignoredItems: [WikidataItemId] = [
                    "Q151",    // Wiktionary
                    "Q2013",   // Wikidata
                    "Q4167410" // Wikimedia disambiguation page
                ]) {
        self.wikidataAPI = WikidataAPI(urlSession: urlSession)
        self.wikipediaAPI = WikipediaAPI(urlSession: urlSession)
        self.ignoredItems = ignoredItems
    }

    public func resolve(title: String, site: String, languages: [String]) -> AnyPublisher<Resolver.Result, Error> {
        guard let primaryLanguage = languages.first else {
            return Fail(error: ResolverError.invalidParameter).eraseToAnyPublisher()
        }
        let siteFilter = Array(Set([site] + languages.map { "\($0)wiki" }))

        return wikidataAPI
            .execute(builder: wikidataQuery(title: title, site: site, siteFilter: siteFilter, languages: languages))
            .flatMap { [unowned self] (response: DataResponse) -> AnyPublisher<Resolver.Result, Error> in
                guard let entities = response.entities, entities.count > 0 else {
                    return Fail(error: ResolverError.notFound).eraseToAnyPublisher()
                }
                func result(entities: [Entity]) -> Result {
                    Result(title: title,
                           normalizedTitle: response.normalized?.to,
                           language: primaryLanguage,
                           entities: entities)
                }

                if entities.isAmbiguous {
                    return self.disambiguate(title: response.normalized?.to ?? title,
                                             language: primaryLanguage).map(result).eraseToAnyPublisher()
                } else {
                    return Just(result(entities: entities)).setFailureType(to: Error.self).eraseToAnyPublisher()
                }
        }.eraseToAnyPublisher()
    }

    private func disambiguate(title: String, language: String) -> AnyPublisher<[Entity], Error> {
        wikipediaAPI
            .execute(builder: wikipediaQuery(title: title, language: language))
            .flatMap { (response: Response) -> AnyPublisher<[Entity], Error> in
                guard let pages = response.query?.pages, pages.count > 0 else {
                    return Fail(error: ResolverError.ambiguous).eraseToAnyPublisher()
                }
                let entities = self.convertToEntities(pages: pages, title: title, language: language)
                return Just(entities).setFailureType(to: Error.self).eraseToAnyPublisher()
            }.eraseToAnyPublisher()
    }

    private func convertToEntities(pages: [Page], title: String, language: String) -> [Entity] {
        pages.filter { page in
            page.ns == 0 && page.has(property: .wikibase_item) && !page.has(property: .disambiguation)
        }.sorted { (page1: Page, page2: Page) -> Bool in
            if page1.has(property: .page_image_free) && !page2.has(property: .page_image_free) {
                return true
            } else {
                return page1.description != nil && page2.description == nil
            }
        }.compactMap { page in
            guard let wikidataItem = page.pageprops?[PageProp.wikibase_item.rawValue],
                  !ignoredItems.contains(wikidataItem) else {
                return nil
            }
            let labels =  [Label(language: language, value: page.title)]
            let descriptions = page.description.map { [Description(language: language, value: $0)] } ?? []
            let sitelink = SiteLink(
                 site: "\(language)wiki",
                 title: page.title,
                 badges: [],
                 url: page.canonicalurl ?? page.fullurl)

            return Entity(type: "item",
                          id: wikidataItem,
                          labels: labels,
                          descriptions: descriptions,
                          sitelinks: [sitelink])
        }
    }

    private func wikidataQuery(title: String,
                               site: String,
                               siteFilter: [String],
                               languages: [String]) -> QueryBuilder {
        WikidataAPIQueryBuilder(action: .wbgetentities)
            .withSite(site)
            .siteFilter(siteFilter)
            .withTitles(title)
            .resolveRedirects()
            .normalizePageTitle()
            .props([.labels, .descriptions, .sitelinksUrls])
            .withLanguages(languages)
            .withLanguageFallback()
    }

    private func wikipediaQuery(title: String, language: String) -> QueryBuilder {
        WikipediaAPIQueryBuilder(language: language, action: .query)
            .withTitles(title)
            .withGenerator(.links)
            .props([.pageprops, .description, .info], infoProps: [.url])
            .pageProps([.wikibase_item, .disambiguation, .page_image_free])
    }

    private var log: OSLog {
        OSLog(subsystem: "com.zegoggles.mica", category: String(describing: Self.self))
    }
}

extension Array where Element == Entity {
    var isAmbiguous: Bool {
        guard count == 1, let entity = first,
              entity.descriptions.contains(where: { description in
                  disambiguationDescriptions[description.language]?.contains(description.value) ?? false
              }) else {
            return false
        }
        return true
    }
}

private let disambiguationDescriptions: [String: [String]] = [
    "en": ["Wikimedia disambiguation page", "Wikipedia disambiguation page"],
    "ca": ["pàgina de desambiguació de Wikimedia"],
    "de": ["Wikimedia-Begriffsklärungsseite"],
    "eo": ["Vikimedia apartigilo"],
    "es": ["página de desambiguación de Wikimedia"],
    "fi": ["Wikimedia-täsmennyssivu"],
    "fr": ["page d'homonymie de Wikimedia", "page d'homonymie d'un projet Wikimédia"],
    "gl": ["páxina de homónimos de Wikimedia"],
    "it": ["pagina di disambiguazione di un progetto Wikimedia"],
    "ko": ["위키미디어 동음이의어 문서"],
    "la": ["pagina discretiva Vicimediorum"],
    "nl": ["Wikimedia-doorverwijspagina"],
    "pt": ["página de desambiguação da Wikimedia", "página de desambiguação de um projeto da Wikimedia"],
    "ru": ["страница значений", "страница значений в проекте Викимедиа"],
    "sv": ["Wikimedia-förgreningssida"]
]

#endif
