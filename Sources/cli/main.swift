import Foundation
import Dispatch
import mica
import ArgumentParser

struct Command: ParsableCommand {
    @Argument(help: "The title to search.")
    var title: String

    @Argument(help: "The language.")
    var language: String

    func run() {
        if #available(macOS 10.15, *) {
            let group = DispatchGroup()
            let resolver = Resolver()
            group.enter()
            let cancellable = resolver.resolve(title: title, site: "\(language)wiki", languages: [language])
                .sink(receiveCompletion: { completion in
                    print(completion)
                    group.leave()
                }, receiveValue: { response in
                    dump(response)
                })
            group.wait()
            cancellable.cancel()
        }
    }
}

Command.main()
