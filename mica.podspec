Pod::Spec.new do |s|
  s.name      = 'mica'
  s.version   = '0.0.1'
  s.summary   = 'A bit of MediaWiki.'
  s.homepage  = 'https://gitlab.com/jberkel/mica'
  s.license   = 'MIT'
  s.author    = { 'Jan Berkel' => 'jan@berkel.fr' }
  s.source    = { :git => 'https://gitlab.com/jberkel/mica.git', :tag =>s.version.to_s }

  s.swift_version = '5.2'
  s.ios.deployment_target = '9.3'
  s.osx.deployment_target = '10.15'

  s.source_files  = ['Sources/mica/**/*.swift']

  s.test_spec do |test_spec|
    test_spec.source_files  = ['Tests/**/*.swift']
    test_spec.dependency 'Quick'
    test_spec.dependency 'Nimble'
  end
end
